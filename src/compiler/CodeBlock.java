package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Currency;

import compiler.CodeBlock.StackFrame;

public class CodeBlock {

	static public class StackFrame {
		int number;
		int ndecls;
		StackFrame parentFrame;
		static int fnumber = 0;
		
		StackFrame(int ndecls, StackFrame parentFrame) {
			this.number = fnumber++;
			this.ndecls = ndecls;
			this.parentFrame = parentFrame;
		}
		
		void dump() {
			// open file
			// write header
			// write ndecls field declarations
			// write footer
		}

		public String getType() {
			return "frame_"+number;
		}
	}

	private static final String SP = "0";
	
	ArrayList<String> code;
	ArrayList<StackFrame> frames;
	StackFrame currentFrame;

	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<StackFrame>();
	}

	public void emit_push(int n) {
		code.add("sipush "+n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}
	
	private void dumpFrames() {
		for(StackFrame f: frames) 
			f.dump();
	}


	public void dump(String filename) throws FileNotFoundException {
		dumpFrames();
		
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
	}

	public StackFrame createFrame(int ndecls) {
		StackFrame frame = new StackFrame(ndecls, currentFrame);
		frames.add(frame);
		return frame;
	}

	public void pushFrame(StackFrame frame) {
		code.add("astore "+SP);
		currentFrame = frame;
	}

	public void popFrame() {
		if(currentFrame.parentFrame == null) {
	       code.add("aconst_null");
	       code.add("astore "+SP);
		}
		else {		
		   code.add("aload "+SP);
	       code.add("checkcast "+currentFrame.getType());
	       code.add("getfield "+
	    		   	    currentFrame.getType()+"/SL "+
	    		   	    this.reftypeToJasmin(currentFrame.parentFrame.getType()));
	       code.add("astore "+SP);
		}	       
	
		currentFrame = currentFrame.parentFrame;
	}

	private String reftypeToJasmin(String type) {
		return "L"+type+";";
	}

	
}