package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTDecl implements ASTNode {
	
	Binding decl;
	ASTNode expr;

    public ASTDecl(Binding decl, ASTNode expr)
    {
		this.decl = decl; 
		this.expr = expr;
    }

    public String toString() {
    		String s = "";
    		s += decl.getId() + " = " + decl.getExpr().toString();
    		return "decl " + s + " in " + expr.toString() + " end";
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		IValue value;

		IValue idValue = decl.getExpr().eval(env);

		Environment<IValue> newEnv = env.beginScope();		
		newEnv.assoc(decl.getId(), idValue);
		value = expr.eval(newEnv);
		newEnv.endScope();
	
		return value;
	}

	@Override
	public void compile(CodeBlock code) {
		// create frame
		StackFrame frame = code.createFrame(1);
		decl.getExpr().compile(code);
		// store value
		// begin scope (push SP)
		code.pushFrame(frame);
		// associate id to address
		expr.compile(code);
		// end scope (pop SP)
		code.popFrame();
	}
}

