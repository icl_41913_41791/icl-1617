package ast.value;

import ast.ASTNode;
import util.Environment;
import values.IValue;

public class Closure implements IValue {

	private String parameter;
	private ASTNode body;
	private Environment<IValue> env;

	public Closure(String parameter, ASTNode body, Environment<IValue> env) {
		this.parameter = parameter;
		this.body = body;
		this.env = env;
	}

	public String getParameter() {
		return parameter;
	}


	public ASTNode getBody() {
		return body;
	}

	public Environment<IValue> getEnv() {
		return env;
	}

}
