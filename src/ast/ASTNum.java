package ast;

import compiler.CodeBlock;
import util.Environment;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {

	int val;

	public IValue eval(Environment<IValue> env) {
		return new IntValue(val);
	}

	public ASTNum(int n) {
		val = n;
	}

	@Override
	public void compile(CodeBlock code) {
		code.emit_push(val);
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}
}
